---
title: Hoofdstuk 5 - Implementatie
weight: 6
---
# Hoofdstuk 5 - Implementatie
Voor ieder van de processen specificeren we in dit hoofdstuk de technische vereisten voor de IT-infrastructuur. Deze specificatie heeft ten doel om interoperabiliteit van de koppelvlakken en protocollen af te spreken evenals het gebruik van standaarden. Deze specificatie heeft niet ten doel om functionaliteit voor de diensten van een product te specificeren. De specificatie van de diensten is voorbehouden aan de ICT-leveranciers van de producten.

In de teksten zijn afnemer en aanbieder als actoren gebruikt. Hiermee bedoelen we de verantwoordelijkheid van de afnemer en de aanbieder. We gaan bij deze verantwoordelijkheden ervan uit dat de bijbehorende activiteiten geautomatiseerd door een systeem zullen worden uitgevoerd.

## ​5.1.​ Overeenkomen informatievraag en technische vraagstelling
Vanuit de technische specificaties worden geen technische eisen gesteld aan dit proces.

## ​5.2.​ Uitgeven gevalideerde vraag
De beheerorganisatie KIK-V implementeert een dienst voor het uitgeven van de gevalideerde vraag. De credential voor de gevalideerde vraag moet uitgegeven worden als een “private” verifiable credential.

Het uitgeven van een credential voor de gevalideerde vraag is vergelijkbaar aan het uitgeven van een organisatie credential zoals [gedocumenteerd](https://nuts-node.readthedocs.io/en/stable/pages/getting-started/4-connecting-crm.html#issue-a-nuts-organization-credential) voor de Nuts-node. In onderstaand voorbeeld is dit weergegeven. Ook het [vertrouwen in de uitgever](https://nuts-node.readthedocs.io/en/stable/pages/getting-started/4-connecting-crm.html#trusting-other-vendors-as-issuer) moet vergelijkbaar geconfigureerd zijn voor het credential van de gevalideerde vraag waarbij de beheerorganisatie KIK-V de uitgever is. Aanbieders moeten daarom de beheerorganisatie KIK-V vertrouwen.

NB: De “visibility” voor het credential moet ingesteld zijn op “private”.

De beheerorganisatie KIK-V is de “issuer” voor het credential, de afnemer van de gevalideerde vraag de “holder”.

Onderstaand voorbeeld is niet normatief. Voor het verzenden van de gevalideerde vraag wordt vooralsnog alleen het attribuut “credentialSubject” gevraagd

```
POST http://example.com/internal/vcr/v2/issuer/vc
Content-Type: application/json

{
    "issuer": "did:nuts:beheerorganisatie_organization_did",
    "type": "ValidatedQueryCredential",
    "issuanceDate": "2021-01-01T00:00:00Z",
    “expirationDate”: "2021-12-31T23:59:59Z",
    "credentialSubject": {
        "id": "did:nuts:did:nuts:afnemer_organisatie_did",
        "validatedQuery": {
            “description”: “CBS/Vernet: Het ziekteverzuimpercentage is het totaal aantal ziektedagen van de personeelsleden, in procenten van het totaal aantal beschikbare (werk-/kalender) dagen van de werknemers in de verslagperiode. Het ziekteverzuimpercentage is inclusief het verzuim langer dan een jaar en exclusief zwangerschaps- en bevallingsverlof.”,
            “identifier”: “Uniek nummer voor de gevalideerde vraag, UUID,geformatteerd overeenkomstig de urn:uuid definitie”,
            “name”: “2.3.1 Ziekteverzuimpercentage”,
            "ontology": [
                "https://gitlab.com/kik-v/ontologie/source/-/raw/2.3.0/onz-g.owl",
                "https://gitlab.com/kik-v/ontologie/source/-/raw/2.3.0/onz-pers.owl",
                "https://gitlab.com/kik-v/ontologie/source/-/raw/2.3.0/onz-zorg.owl",
                "https://gitlab.com/kik-v/ontologie/source/-/raw/2.3.0/onz-fin.owl",
                "https://gitlab.com/kik-v/ontologie/source/-/raw/2.3.0/onz-org.owl"
                ],
            “paramsSHACL”: “...”,
            "profile": "https://kik-v-publicatieplatform.nl/documentatie/uitwisselprofiel-odb/",

            "sparql": "...",
        }
    },
    "publishToNetwork": true,
    "visibility": "private"
}
```

### ​5.2.1​. Parameters voor de SPARQL-query
Voor een SPARQL-query kunnen parameters gedefinieerd zijn. Bijvoorbeeld om een peildatum, een periode of een kwartaal aan te duiden. De definitie van de parameter(s) die binnen de betreffende sparql query gebruikt mogen worden, worden opgenomen binnen het "paramsSHACL" veld onderdeel van het "ValidatedQueryCredential". De uiteindelijk gekozen waarden voor de parameters worden opgenomen in het param_values veld. De query wordt uitgestuurd waarbij dus de waarden in param_values in ttl formaat opgenomen zijn. Deze waarde kan vervolgens geverifiëerd worden met de paramSHACL waarde vanuit de ValidatedQueryCredential.


Onderstaande uitgangspunten worden gehanteerd voor de specifieke parameter definities.
De definitie is beschreven in de Shapes Constraint Language (SHACL).

NB: Voorbeelden dienen als niet normatief te worden beschouwd. 



| SHACL Veld        | Beschrijving                                                                 | Voorbeeld Waarde                        |
|--------------------|-----------------------------------------------------------------------------|-----------------------------------------|
| ex:NameEntry   | De naam van de shape of entiteit die wordt gedefinieerd.                   | ex:PeilDatumManualEntry                 |
| a sh:NodeShape | Bepaalt dat dit een SHACL NodeShape is, een structuur die eigenschappen van data definieert. | sh:NodeShape                            |
| sh:targetClass | Doelklasse waarop deze shape van toepassing is.                            | ex:QueryParameter                       |
| rdfs:label     | Een beschrijvend label voor de shape.                                      | "Bijv. PeilDatum without a pick list"   |
| rdfs:comment   | Een beschrijving van de shape voor extra uitleg of context.               | "Een parameter waarbij via handmatige invoer een datum opgave gedaan kan worden."@nl |
| sh:property    | Definieert de eigenschappen die binnen de shape moeten worden gevalideerd. | Zie hieronder voor uitleg van subvelden |
| sh:path        | Het pad naar de specifieke eigenschap waarop de restricties van toepassing zijn. | ex:peildatum                            |
| sh:name        | De naam van de eigenschap die wordt gevalideerd.                          | "peildatum"                             |
| sh:description | Een beschrijving van wat de eigenschap betekent of hoe deze moet worden gebruikt. | "Peildatum via manuele datum opgave."@nl|
| sh:maxCount    | Het maximale aantal waarden dat is toegestaan voor deze eigenschap.       | 1                                       |
| sh:minCount    | Het minimale aantal waarden dat is vereist voor deze eigenschap.          | 1                                       |
| sh:datatype    | Het datatype dat voor deze eigenschap vereist is.                         | xsd:date                                |
| sh:in          | De mogelijke waarde(s) die meegegeven zouden kunnen worden en valide zijn.| Q1, Q2, Q3, Q4                          |
| sh:pattern     | Het patroon waaraan een waarde moet voldoen.                              | KvK vestigingsnummer: 000026641704      |
| sh:maxInclusive| De maximale toegestane waarde voor de eigenschap.                         | "2025-12-31"^^xsd:date                  |
| sh:minInclusive| De minimale toegestane waarde voor de eigenschap.                         | "2020-12-31"^^xsd:date                  |


NB: De waarden voor sh:path en sh:name zijn hoofdlettergevoelig (case sensitive). Binnen deze context worden ze altijd met kleine letters (lowercase) aangeduid en meegestuurd. Hierbij is de sh:name de bepalende waarde waarop gekoppeld wordt.

Hieronder een voorbeeld van een sparql query waarin de parameter ?peildatum is opgenomen, de wijze waarop dit binnen deze query gedaan is, is illustratief voor gebruik van overige parameters. De naam van de parameter wordt als volgt opgenomen in de SPARQL: ?peildatum
Voor het onderstaande voorbeeld is dit dus: ?peildatum. 

De aanbieder moet de ontvangen parameters dus verifiëren op basis van de SHACL uit de gevalideerde vraag en de ontvangen waarden voor de parameters opnemen in de SPARQL Query. 

Dus als er binnen een query sprake is van een parameter wordt dat in de tweede regel via commentaar aangegeven en aangeduid met #Parameters: ?parameter(s). 

```
#Indicator: IGJ 1.1.1
#Parameters: ?peildatum
#Ontologie: versie 2.0.0 of nieuwer

PREFIX onz-g: <http://purl.org/ozo/onz-g#>
PREFIX onz-org: <http://purl.org/ozo/onz-org#>
PREFIX onz-zorg: <http://purl.org/ozo/onz-zorg#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT 
	?zorgprofiel
    (COUNT (DISTINCT(?client)) AS ?indicator)
WHERE
{ 
    VALUES ?zorgprofiel { onz-zorg:4VV onz-zorg:5VV onz-zorg:6VV onz-zorg:7VV onz-zorg:8VV onz-zorg:9BVV onz-zorg:10VV }

    #selecteer de zorgprocessen op de vestiging die voldoen aan de inclusiecriteria
    ?zorgproces
        a onz-zorg:NursingProcess ;
        onz-g:definedBy ?indicatie ;
        onz-g:startDatum ?start_zorgproces .
    	OPTIONAL {?zorgproces onz-g:eindDatum ?eind_zorgproces}
    	FILTER (?start_zorgproces <= ?peildatum && ((?eind_zorgproces >= ?peildatum)||(!BOUND(?eind_zorgproces))))
    #selecteer de bijbehordene indicatie, om unieke clienten te kunnen bepalen
    ?indicatie 
        onz-g:hasPart ?zorgprofiel ;
        onz-g:isAbout ?client .
    ?client a onz-g:Human .
} 
GROUP BY ?zorgprofiel
ORDER BY ?zorgprofiel
```

Vooralsnog zijn volgende paramsSHACLS beschreven:
- Peildatum met handmatige invoer
- Peildatum met keuzelijst
- Kwartaalwaardes met keuzelijst
- Kwartaalwaardes met keuzelijst incl. jaar
- Starperiode met handmatige invoer
- Eindperiode met handmatige invoer
- Gecombineerde parameter voor start- en eindperiode
- Vestigingsnummer
- Zorgkantoor

Hieronder worden deze verschillende paramsSHACLS verder toegelicht.  


### Peildatum met handmatige invoer.
Parameter voor peildatum via manuele datum opgave.

```
# Parameter-shapes voor peildatum met handmatige invoer als een datum.
ex:PeilDatumManualEntry
    a sh:NodeShape ;
    sh:targetClass ex:PeilDatumManualEntry ;
    rdfs:label "Peildatum zonder keuzelijst";
    rdfs:comment "Een shape voor het creëren van een handmatig invoerveld waarmee de gebruiker de peildatum kan specificeren.."@nl ;
    sh:property [
          sh:path ex:peildatum ;
          sh:name "peildatum";
          sh:description "Handmatige invoer van peildatumwaarden."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:date ;
          sh:maxInclusive "2025-12-31"^^xsd:date ;
          sh:minInclusive "2025-01-01"^^xsd:date ;
      ]
.

```


### Peildatum met keuzelijst.
Parameters waarmee een peildatum uit een specifieke keuzelijst geselecteerd kan worden. 

```
# Parameter-shapes voor peildatum met een keuzelijst.
ex:PeilDatumPickList
    a sh:NodeShape ;
    sh:targetClass ex:PeilDatumPickList ;
    rdfs:label "Peildatum met een keuzelijst";
    rdfs:comment "Een shape voor het creëren van een keuzelijstveld waarmee de gebruiker de peildatum kan specificeren."@nl ;
    sh:property [
          sh:path ex:peildatum ;
          sh:name "peildatum";
          sh:description "Keuzelijst van peildatumwaarden."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:date ;
          sh:in ( 
                "2025-01-01"^^xsd:date 
                "2025-04-01"^^xsd:date 
                "2025-07-01"^^xsd:date 
                "2025-10-01"^^xsd:date 
            )

      ]
.

```


### Kwartaalwaardes met keuzelijst inclusief jaar.
Via deze paramsSHACL shape wordt het mogelijk zowel de parameter van het kwartaal als jaar te definiëren.

```
ex:QuarterValuePickListWithYear
    a sh:NodeShape ;
    sh:targetClass ex:QuarterValuePickListWithYear ;
    rdfs:label "Kwartaal- en jaarwaarden" ;
    rdfs:comment "Een shape met twee eigenschappen: één om een keuzelijstveld te creëren waarmee de gebruiker een filter op een kwartaal kan specificeren, en een tweede voor handmatige invoer om het jaar te specificeren."@nl ;
    sh:property [
          sh:path ex:kwartaal ;
          sh:name "kwartaal";
          sh:description "De keuzelijst met waarden voor kwartalen."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:string ;
          sh:in ("Q1" "Q2" "Q3" "Q4")
      ] ;
    sh:property [
          sh:path ex:jaar ;
          sh:name "jaar";
          sh:description "Handmatige invoer van de jaarwaarde na de kwartaalwaarden."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:gYear ;
          sh:maxInclusive "2025"^^xsd:gYear ;
          sh:minInclusive "2025"^^xsd:gYear ;
      ]
.

```


### Startperiode met handmatige invoer.
Via deze parameter wordt het mogelijk om een begin (start) periode waarde op te geven.

```
# De volgende SHACL shape definieert de invoerparameters voor de startperiode..
ex:StartDateManualEntry
    a sh:NodeShape ;
    sh:targetClass ex:StartDateManualEntry ;
    rdfs:label "Startdatum zonder keuzelijst";
    rdfs:comment "Een shape voor het creëren van een handmatig invoerveld waarmee de gebruiker de filter voor de startdatum kan specificeren."@nl ;
    sh:property [
          sh:path ex:startperiode ;
          sh:name "startperiode";
          sh:description "Handmatige invoer van de startdatumwaarde voor de periode."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:date ;
          sh:maxInclusive "2025-12-31"^^xsd:date ;
          sh:minInclusive "2025-01-31"^^xsd:date ;
      ]
.
```


### Eindperiode met handmatige invoer.
Via deze parameter wordt het mogelijk om een eind periode waarde op te geven.

```
# De volgende SHACL shape definieert de invoerparameters voor de eindperiode.
ex:EndDateManualEntry
    a sh:NodeShape ;
    sh:targetClass ex:EndDateManualEntry ;
    rdfs:label "Einddatum zonder keuzelijst";
    rdfs:comment "Een shape voor het creëren van een handmatig invoerveld waarmee de gebruiker het filter op de einddatum kan specificeren."@nl ;
    sh:property [
          sh:path ex:eindperiode ;
          sh:name "eindperiode";
          sh:description "Handmatige invoer van de einddatumwaarde voor de periode."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:date ;
          sh:maxInclusive "2025-12-31"^^xsd:date ;
          sh:minInclusive "2020-12-31"^^xsd:date ;
      ]
.
```


### Gecombineerde parameter voor start-, eindperiode.

Via deze paramsSHACL is een gecombineerde parameter van start- en eindperiode op te geven.

```
# De volgende SHACL shapes definiëren de invoerparameters voor de start- en einddatums samen.
ex:StartAndEndDateManualEntry
    a sh:NodeShape ;
    sh:targetClass ex:StartAndEndDateManualEntry ;
    rdfs:label "Start- en einddatum zonder keuzelijst";
    rdfs:comment "Een shape met twee eigenschappen met dezelfde formaten: één voor het creëren van een handmatig invoerveld waarmee de gebruiker de filter voor de startdatum kan specificeren, en een tweede voor de einddatum."@nl ;
    sh:property [
          sh:path ex:startperiode ;
          sh:name "startperiode";
          sh:description "Handmatige invoer van de startdatumwaarde."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:date ;
          sh:maxInclusive "2025-12-31"^^xsd:date ;
          sh:minInclusive "2020-12-31"^^xsd:date ;
      ],[
          sh:path ex:eindperiode ;
          sh:name "eindperiode";
          sh:description "Handmatige invoer van de einddatumwaarde."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:date ;
          sh:maxInclusive "2025-12-31"^^xsd:date ;
          sh:minInclusive "2020-12-31"^^xsd:date ;
      ]
.
```


### Vestigingsnummer.

Via deze parameter kan het KVK Vestigingsnummer worden opgenomen binnen de query.

```
# De volgende SHACL shape definieert de invoerparameter voor Vestigingsnummer met handmatige invoer als een string..
ex:VestigingsnummerManualEntry
    a sh:NodeShape ;
    sh:targetClass ex:VestigingsnummerManualEntry ;
    rdfs:label "Vestigingsnummer voor handmatige invoer";
    rdfs:comment "Een shape voor het creëren van een handmatig invoerveld waarmee de gebruiker het KvK Vestigingsnummer kan specificeren."@nl ;
    sh:property [
          sh:path ex:vestigingsnummer ;
          sh:name "vestigingsnummer";
          sh:description "Handmatige invoer van het KvK vestigingsnummer."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:string ;  
          sh:pattern "^[0-9]{12}$" ; 
.
```


### Zorgkantoor.
Deze parameter maakt het mogelijk om binnen de queries die vestigingen te bepalen waarvoor het betreffende zorgkantoor consessiehouder is en dus antwoorden mag retourneren. 

```
# De volgende SHACL shape definieert de invoerparameter voor Zorgkantoor IRI.
ex:Zorgkantoor
    a sh:NodeShape ;
    sh:targetClass ex:Zorgkantoor ;
    rdfs:label "Zorgkantoor" ;
    rdfs:comment "Een shape voor het kunnen meegeven van de specifieke zorgkantoor IRI."@nl ;
    sh:property [
          sh:path ex:zorgkantoor ;
          sh:name "zorgkantoor" ;
          sh:description "De betreffende IRI van het zorgkantoor."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:nodeKind sh:IRI ;
          sh:in (
              <http://purl.org/ozo/onz-org#ZorgkantoorCZ>
              <http://purl.org/ozo/onz-org#ZorgkantoorDSW>
              <http://purl.org/ozo/onz-org#ZorgkantoorFriesland>
              <http://purl.org/ozo/onz-org#ZorgkantoorMenzis>
              <http://purl.org/ozo/onz-org#ZorgkantoorMiddenIJssel>
              <http://purl.org/ozo/onz-org#ZorgkantoorVGZ>
              <http://purl.org/ozo/onz-org#ZorgkantoorZilverenKruis>
              <http://purl.org/ozo/onz-org#ZorgkantoorZorgEnZekerheid>
          ) ;
    ]
.

```

Voor de volledige lijst van deze parameters wordt verwezen naar dit [document](/Documentatie/Implementatie/AllParameters_def.md).

### ​5.2.2.​ Intrekken van een gevalideerde vraag
De beheerorganisatie KIK-V kan als uitgever op ieder moment een gevalideerde vraag intrekken. De gevalideerde vraag is vanaf dat moment niet meer geldig. Een aanbieder verifieert een gevalideerde vraag met als peildatum de datum en tijd van ontvangst. Als een gevalideerde vraag op het moment van ontvangst geldig is (met een leashtime van 10 seconden), dan moet deze worden verwerkt.

### ​5.3.​ Uitgeven uittreksel zorgaanbiedergegevens
Het uittreksel wordt ontwikkeld door het team dat de uitgifte van verifieerbare referentie gaat ontwikkelen (zie paragraaf 7. Onderwerpen voor doorontwikkeling). Het uittreksel zal minimaal het KvK-nummer van de zorginstelling en het AGB-nummer van de zorginstelling moeten bevatten.

## ​5.4.​ Beschikbaar stellen data en adverteren endpoints

### 5.4.1.​ Beschikbaar stellen data
De aanbieder moet de data voor de informatievragen beschikbaar stellen in een RDF-database (triple store). Hiervoor moet de aanbieder een RDF-database implementeren naar eigen keuze. RDF staat voor Resource Description Framework en is een standaard voor het beschrijven van informatie en kennis. De aanbieder realiseert koppelingen met haar toepassingen en informatiesystemen om de data beschikbaar te stellen in de RDF-database.

De aanbieder implementeert een RDF-database met de volgende eisen:
1. De RDF-database moet minimaal versie 1.1 van het Resource Description Framework gebruiken voor de representatie van de kennisgraaf.
2. De RDF-database moet ondersteuning bieden aan OWL2 RL voor redenatie op de kennisgraaf.
3. De RDF-database moet ondersteuning bieden aan SHACL voor de toepassing van kennisregels.

De aanbieder moet de ontologie voor de domeinen waarin de aanbieder werkzaam is opnemen in de RDF-database. Zie hiervoor [Afsprakenset](https://kik-v-publicatieplatform.nl/afsprakenset/_/content/docs/modelgegevensset/publicatie). De ontologie is beschreven in de W3C Web Ontology Language (OWL2).

### ​5.4.2​. Beschikbaar stellen stellen SPARQL-endpoint
De aanbieder moet een SPARQL-endpoint implementeren zodat een RDF-database bevraagd kan worden. Dit is een intern endpoint en zou niet toegankelijk moeten zijn voor afnemers van gegevens. Het SPARQL-endpoint moet de technische vraagstelling kunnen uitvoeren. Het antwoord van de SPARQL-endpoint moet overeenkomstig versie 1.1 van de SPARQL-resultaten zijn, zie [https://www.w3.org/TR/sparql11-results-json/](https://www.w3.org/TR/sparql11-results-json/).

De aanbieder hanteert voor haar RDF de volgende goede praktijken:
* Wereldwijd uniek: een URI mag nooit naar twee verschillende concepten verwijzen, zelfs niet naar concepten die gelijk lijken.
* Aanhoudend: een URI zou moeten blijven bestaan. 
* Stabiel: een URI zou nooit opnieuw gebruikt mogen worden voor verschillende dingen tussen versies, zelfs als het origineel is verwijderd.
* Oplosbaar: wanneer een gebruiker op een URI in zijn browser klikt, zou deze omgeleid moeten worden naar een geschikt document (Linked Data). Dit betekent niet noodzakelijk dat het in staat moet zijn om RDF-inhoud te retourneren.

Meer goede praktijken zijn te vinden op: [https://www.w3.org/TR/cooluris/](https://www.w3.org/TR/cooluris/).

### ​5.4.3.​ Beschikbaar stellen berichtendienst
De aanbieder moet een berichtendienst (zie aanvullende eisen) beschikbaar stellen voor de ontvangst van gevalideerde vragen. Het endpoint van de berichtendienst moet geadverteerd worden in het Nuts-netwerk.

## ​5.5.​ Vinden zorginstelling
De technische specificaties stellen als uitgangspunt dat het vinden van een zorginstelling resulteert in een KvK-nummer. Het vinden van de zorginstelling zelf is geen onderdeel van de technische specificatie. Hiervoor moet een afnemer zelf een voorziening realiseren wat resulteert in een lijst van KvK-nummers.  

Het uittreksel zorgaanbiedergegevens moet zijn uitgegeven voor een zorginstelling om op basis van het KvK-nummer een zorginstelling in Nuts te kunnen vinden. Het uittreksel moet de afnemer in staat stellen om betrouwbaar een endpoint te vinden.

| Let op! |
|---|
| Zolang het uittreksel zorgaanbiedergegevens niet beschikbaar is, zal het vinden van een zorginstelling moeten resulteren in de naam die eveneens in de Nuts nodes is geregistreerd. Deze registratie vindt plaats op basis van de NutsOrganizationCredential. Een afnemer kan daardoor in het register van Nuts zoeken naar de naam van de zorginstelling om een adres (een endpoint) te vinden. Uitgangspunt is dat een zorginstelling alleen in Nuts gevonden kan worden als die is aangesloten op Nuts. |

## ​5.6.​ Verzenden gevalideerde vraag
De vragende partij (“de afnemer”) moet een bericht verzenden naar de berichtendienst van de aanbieder. In het bericht wordt de gevalideerde vraag gepresenteerd. 

In onderstaand diagram is de flow weergegeven voor het verzenden van de gevalideerde vraag door de afnemer.

![Verzenden gevalideerde vragen](Afbeelding_4.png)
*Figuur 4. Verzenden gevalideerde vragen*

De stappen voor het vinden van de berichtendienst en het verzenden van een bericht zijn beschreven in de aanvullende specificaties voor de berichtendienst (zie paragraaf 6. Aanvullende specificaties). In deze paragraaf wordt alleen een beschrijving gegeven van de inhoud van het bericht.

### 5.6.1​. Verzamelen gevalideerde vragen
De afnemer verzamelt de gevalideerde vragen die zij heeft ontvangen van de beheerorganisatie KIK-V. Uit deze verzameling kiest zij welke gevalideerde vraag ze wil stellen aan de aanbieder. Zie de Nuts [API-specificatie](https://nuts-node.readthedocs.io/en/stable/pages/integrating/api.html) om verifieerbare referenties te kunnen vinden.

### ​5.6.2.​ Aanmaken presentatie voor de gevalideerde vraag
De afnemer moet de geselecteerde gevalideerde vragen presenteren aan de aanbieder. Hiervoor moet de afnemer een verifiable presentation aanmaken die elektronisch is verzegeld door de afnemer zelf. De afnemer toont hiermee cryptografisch aan de eigenaar te zijn van de gevalideerde vraag. De afnemer moet de gevalideerde vraag presenteren met hetzelfde “did” waarmee de afnemer de gevalideerde vraag heeft verkregen. Voor meer informatie, zie [https://www.w3.org/TR/vc-data-model/#presentations](https://www.w3.org/TR/vc-data-model/#presentations).

Met onderstaand voorbeeld wordt door een Nuts-node een verifiable presentation aangemaakt. De afnemer moet hiervoor haar eigen Nuts-node gebruiken.

```
POST http://example.com/internal/vcr/v2/holder/vp
Content-Type: application/json

{
    "verifiableCredentials":[
         // de ValidatedQueryCredential zoals verkregen
    ],
    "signerDID" : "did:nuts:afnemer_organization_did",
    "proofPurpose":"assertionMethod",
    "challenge":"een willekeurige challenge",
    "expires":"expiratiedatum in ISO 8601 (example: 2022-12-20T09:00:00Z)"
}
```

Als expiratiedatum wordt een termijn van 10 minuten aangehouden. Binnen deze termijn moet een aanbieder de presentatie hebben geverifieerd. Als de expiratiedatum is verstreken, dan moeten aanbieders de presentatie afkeuren.

Een afnemer kan met een presentatie meerdere gevalideerde vragen verzenden in een presentatie. 

### ​5.6.3.​ Opnemen parameterwaarden in het bericht (In ontwikkeling)
Als de SPARQL een parameter bevat die ingevuld moet worden, dan moet de afnemer de waarden voor die parameter opgeven in het bericht. Zie hiervoor [6.1.3 Samenstellen inhoud bericht](/Documentatie/Aanvullende%20specificaties#6.1.3.-samenstellen-inhoud-bericht). De afnemer moet voor iedere parameter een geldige waarde opgeven. De technische vraag wordt namelijk pas uitvoerbaar als de combinatie van de SPARQL en de parameters een geldige query oplevert. De afnemer geeft de parameters op in JSON-LD-formaat overeenkomstig de SHACL-definitie van de gevalideerde vraag. 
Voor een voorbeeld, zie [5.7.1 Ontvangen bericht](/Documentatie/Implementatie#5.7.1.-ontvangen-bericht).

### ​5.6.4​. Verzenden bericht
De presentatie van de gevalideerde vragen of vragen worden in de body van een bericht verzonden. Zie hiervoor paragraaf [6.1 beschikbaar stellen van de berichtendienst](/Documentatie/Aanvullende%20specificaties#6.1.-verzenden-berichten-via-de-berichtendienst). De afnemer verzendt het bericht via de berichtendienst.

### 5.6.5. Registreren verzonden bericht in het logboek
De afnemer registreert ieder verzonden bericht in een logboek. Zie hiervoor de paragraaf over het [logboek van ontvangen en verzonden berichten](/Documentatie/Aanvullende%20specificaties#6.2.-logboek-van-ontvangen-en-verzonden-berichten).

## 5.7.​ Ontvangen en beantwoorden gevalideerde vraag
### ​5.7.1​. Ontvangen bericht
De aanbieder ontvangt de gevalideerde vraag in het bericht van de berichtendienst en verwerkt deze. Zie de paragraaf over het [beschikbaar stellen van de berichtendienst](/Documentatie/Aanvullende%20specificaties#6.1.-verzenden-berichten-via-de-berichtendienst) en de [beveiliging van de berichtendienst](/Documentatie/Aanvullende%20specificaties#6.4.-beveiliging-van-de-berichtendienst) zodat alleen berichten worden ontvangen met een geldig access token. Als het access token niet geldig is, wordt het bericht niet geaccepteerd en is het niet ontvangen. De afnemer ontvangt een html foutcode.

Een ontvangen bericht moet een unieke identificatie (id) hebben, in het formaat van uuid v4 (geformatteerd overeenkomstig de urn:uuid definitie), dat nog niet eerder is ontvangen. Als een bericht met het id al bestaat, dan wordt het nieuw ontvangen bericht afgekeurd en zal een foutbericht worden verzonden. 

De verwerking van een ontvangen bericht voor het “validated-query-request”-protocol is hieronder beschreven. Het zijn alleen stappen in de verwerking die afgesproken zijn. Naast deze afgesproken verwerkingen kan de aanbieder besluiten aanvullende stappen uit te voeren in de verwerking van een ontvangen bericht. 

### ​5.7.2.​ Registreren ontvangen bericht in het logboek
De aanbieder registreert ieder ontvangen bericht in een logboek. Zie de paragraaf over het [logboek van ontvangen en verzonden berichten](/Documentatie/Aanvullende%20specificaties#6.2.-logboek-van-ontvangen-en-verzonden-berichten).

### ​5.7.3.​ Verifiëren herkomst van het bericht
De aanbieder verifieert dat de verzender autorisatie heeft aangevraagd en geautoriseerd is voor de scope “didcomm-service-kikv” in het access token. De verzender is geïdentificeerd door het attribuut “from” in het bericht.

De aanbieder moet verifiëren dat de verzender (geïdentificeerd door het attribuut “from”) gelijk is aan het onderwerp van de access token (geïdentificeerd door het attribuut “sub”).

De aanbieder moet verifiëren dat het bericht voor haar bestemd is. De aanbieder verifieert dat de ontvanger (geïdentificeerd door het attribuut “to”) gelijk is aan de uitgever van de access token (geïdentificeerd door het attribuut “iss”).

(MVP: Door het ontbreken van de presentatie in de MVP, moet de aanbieder verifiëren dat het bericht verzonden is door de KIK-Starter. De verificatie dat het bericht afkomstig is van de KIK-Starter wordt uitgevoerd door een verificatie op de did van de KIK-Starter)

### ​5.7.4.​ Verifiëren presentatie van de gevalideerde vraag
​De aanbieder moet verifiëren dat de ontvangen presentatie van gevalideerde vragen geldig is, evenals de gevalideerde vragen die gepresenteerd worden. Hierbij moet de aanbieder verifiëren dat de gevalideerde vragen uitgegeven zijn door de beheerorganisatie KIK-V en dat de attributen niet gewijzigd zijn sinds de uitgifte van de gevalideerde vraag. Al deze verificaties kunnen door de Nuts-node van de aanbieder worden uitgevoerd. In onderstaand voorbeeld is te zien hoe.

```
POST http://example.com/internal/vcr/v2/verifier/vp
Content-Type: application/json

{
    "verifiablePresentation": 
    {
        // de verifiable presentation die ontvangen is
    }
}
```

De presentatie wordt gebruikt om houderschap van de gevalideerde vraag te bewijzen. De presentatie moet daarom de did van de houder van de gevalideerde vraag bevatten in het attribuut “holder” van de presentatie. 
De aanbieder moet verifiëren dat de presentatie ondertekend is door de houder (geïdentificeerd door het attribuut “holder”). 

De aanbieder moet verifiëren dat de gevalideerde is uitgegeven aan de houder die de gevalideerde vraag presenteert. De houder van de gevalideerde vraag wordt geïdentificeerd door het attribuut “id” in de “credentialSubject” van een gevalideerde vraag.

De aanbieder moet verifiëren dat de gevalideerde vraag is ontvangen in een bericht afkomstig van de houder (geïdentificeerd door het attribuut “from” van een ontvangen bericht).

De aanbieder zou een geldige gevalideerde vraag moeten verwerken en de technische vraagstelling uitvoeren. De technische vraagstelling is in de gevalideerde vraag opgenomen. 

De aanbieder mag een controle uitvoeren dat de vraagstelling overeenkomt met de gepubliceerde vraagstellingen. Indien de vraagstelling niet overeenkomt met de gepubliceerde vraagstellingen, dan moet de aanbieder de vraagstelling weigeren uit te voeren.

#### ​5.7.4.1.​ Verifiëren van de ontvangen parameters
Als een gevalideerde vraag parameters bevat voor de SPARQL-query, dan moet de aanbieder de ontvangen parameters verifiëren. De verificatie moet uitgevoerd worden met de SHACL-definitie. 

### 5.7.5.​ Uitvoeren van de technische vraagstelling (In ontwikkeling)
Als de parameters onderdeel van de vraagstelling worden, zal overwogen worden om het bericht elektronisch te ondertekenen om de herkomst en integriteit van het bericht te garanderen.
De technische vraagstelling is beschreven in SPARQL. Als de gevalideerde vraag parameters bevat, dan moet de aanbieder de parameters invullen in de SPARQL. De parameters hebben in de SPARQL een placeholder met het volgende format: ?(naam van de parameter). De placeholder moet vervangen worden door de ontvangen waarde voor de parameter.

Voorbeeld van ontvangen waarde voor een parameter:

```
"param_values":
"
@prefix dash:   <http://datashapes.org/dash#> .
@prefix ex:     <http://example.com/> .
@prefix foaf:   <http://xmlns.com/foaf/0.1/> .
@prefix rdfs:   <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema: <http://schema.org/> .
@prefix sh:     <http://www.w3.org/ns/shacl#> .
@prefix vcard:  <http://www.w3.org/2006/vcard/ns#> .
@prefix xsd:    <http://www.w3.org/2001/XMLSchema#> .

ex:PeilDatumManualEntry
        a             
		ex:QueryParameter ;
        ex:peildatum  "2024-01-31"^^xsd:date ."
```

De aanbieder voert vervolgens de SPARQL uit op de SPARQL-endpoint die gekoppeld is aan de RDF-database van de aanbieder.  Op basis van de attributen uit de gevalideerde vraag moet de aanbieder in staat zijn de juiste dataset voor de query te selecteren.

Het antwoord van de SPARQL-endpoint moet overeenkomstig versie 1.1 van de SPARQL-resultaten zijn, zie [https://www.w3.org/TR/sparql11-results-json/](https://www.w3.org/TR/sparql11-results-json/).

### ​5.7.6.​ Verzenden bericht
De aanbieder moet het antwoord (van de SPARQL) opnemen in een key-value array bestaande uit de identificatie van de vraag en het resultaat van de SPARQL. De resultset is een array omdat er meerdere gevalideerde vragen in een presentatie kunnen zijn ontvangen.

Voor iedere antwoord in de array wordt een unieke identificatie meegegeven van de gevalideerde vraag waar het antwoord betrekking op heeft. Deze identificatie is een samenstelling van het unieke id van het bericht en het id van de gevalideerde vraag met als scheidingsteken een “#”.

```
"resultset": [
    {
        "id" : "5bb9dc29-9213-4111-917e-44ca04602ad9#c23ba5eb-112a-4dc1-939e-3baa0d2b05d6",
        "result" : "query result"
    }
]
```

De verzender moet de resultset elektronisch verzegelen met een JSON Web Signature (JWS) voor onweerlegbaarheid van herkomst. Hiervoor wordt de resultset opgenomen als payload van de JWS. 

De verzender moet voor de verzegeling een private sleutel gebruiken waarvan de publieke sleutel is gepubliceerd in het did-document van de verzender als een van de verificatiemethoden (”verificationMethod”). Dit om verificatie door de ontvanger mogelijk te maken. Door middel van de key identifier (kid) van de JWS kan de ontvanger de publieke sleutel achterhalen en de zegel verifiëren. Voor de versleuteling wordt het default algoritme van Nuts gehanteerd.

De aanbieder verzendt het bericht via de berichtendienst. Zie hiervoor [Verzenden berichten via de berichtendienst](/Documentatie/Aanvullende%20specificaties#6.1.-verzenden-berichten-via-de-berichtendienst).

### 5.7.7.​ Registreren verzonden bericht in het logboek
De aanbieder registreert ieder verzonden bericht in een logboek. Zie hiervoor de paragraaf over het [logboek van ontvangen en verzonden berichten](/Documentatie/Aanvullende%20specificaties#6.2.-logboek-van-ontvangen-en-verzonden-berichten).

## ​5.8.​ Ontvangen antwoord op gevalideerde vraag
De afnemer ontvangt de resultset in het bericht van de berichtendienst en verwerkt deze. Zie de paragraaf over het [beschikbaar stellen van de berichtendienst](/Documentatie/Aanvullende%20specificaties#6.1.-verzenden-berichten-via-de-berichtendienst) en de [beveiliging van de berichtendienst](/Documentatie/Aanvullende%20specificaties#6.4.-beveiliging-van-de-berichtendienst) zodat alleen berichten worden ontvangen met een geldig access token. Als het access token niet geldig is, wordt het bericht niet geaccepteerd en is het niet ontvangen. De aanbieder ontvangt een html foutcode.

De verwerking van een ontvangen bericht voor het “validated-query-request”-protocol is hieronder beschreven. Het zijn alleen stappen in de verwerking die afgesproken zijn. Naast deze afgesproken verwerkingen kan de afnemer besluiten aanvullende stappen uit te voeren in de verwerking van een ontvangen bericht. 

Het is toegestaan om meerdere antwoorden te ontvangen op een verzonden bericht. Een verzonden bericht kan namelijk meerdere gevalideerde vragen bevatten die verschillend in tijd beantwoord worden. Ook is het toegestaan dat een vraag meerdere keren wordt beantwoord zodat correcties doorgegeven kunnen worden. Het laatste antwoord is het geldige actuele antwoord.

De afnemer moet de elektronische zegel van de aanbieder verifiëren. Zie voor de sleutel waarmee de zegel is geplaatst de paragraaf over het verzenden van het bericht.

De afnemer moet de antwoorden correleren met de vraag op basis van de ‘id’ in de resultset. Zie hiervoor de paragraaf over het verzenden van het bericht.

### ​5.8.1.​ Registreren ontvangen bericht in het logboek
De afnemer registreert ieder ontvangen bericht in een logboek. Zie de paragraaf over het [logboek van ontvangen en verzonden berichten](/Documentatie/Aanvullende%20specificaties#6.2.-logboek-van-ontvangen-en-verzonden-berichten).

### ​5.8.2.​ Verifiëren elektronische handtekening
De afnemer verifieert dat de elektronische handtekening een geldige handtekening is van de aanbieder voor de resultaatset.
