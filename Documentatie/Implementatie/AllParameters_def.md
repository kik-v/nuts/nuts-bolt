# Alle parameters 
Hieronder zijn alle parameters opgenomen.

```
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix schema: <http://schema.org/> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix dash: <http://datashapes.org/dash#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix vcard: <http://www.w3.org/2006/vcard/ns#> .
@prefix ex: <http://example.com/> .

# Parameter-shapes voor peildatum met handmatige invoer als een datum.
ex:PeilDatumManualEntry
    a sh:NodeShape ;
    sh:targetClass ex:PeilDatumManualEntry ;
    rdfs:label "Peildatum zonder keuzelijst";
    rdfs:comment "Een shape voor het creëren van een handmatig invoerveld waarmee de gebruiker de peildatum kan specificeren.."@nl ;
    sh:property [
          sh:path ex:peildatum ;
          sh:name "peildatum";
          sh:description "Handmatige invoer van peildatumwaarden."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:date ;
          sh:maxInclusive "2025-12-31"^^xsd:date ;
          sh:minInclusive "2025-01-01"^^xsd:date ;
      ]
.

# Parameter-shapes voor peildatum met een keuzelijst.
ex:PeilDatumPickList
    a sh:NodeShape ;
    sh:targetClass ex:PeilDatumPickList ;
    rdfs:label "Peildatum met een keuzelijst";
    rdfs:comment "Een shape voor het creëren van een keuzelijstveld waarmee de gebruiker de peildatum kan specificeren."@nl ;
    sh:property [
          sh:path ex:peildatum ;
          sh:name "peildatum";
          sh:description "Keuzelijst van peildatumwaarden."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:date ;
          sh:in ( 
                "2025-01-01"^^xsd:date 
                "2025-04-01"^^xsd:date 
                "2025-07-01"^^xsd:date 
                "2025-10-01"^^xsd:date 
            )

      ]
.

ex:QuarterValuePickListWithYear
    a sh:NodeShape ;
    sh:targetClass ex:QuarterValuePickListWithYear ;
    rdfs:label "Kwartaal- en jaarwaarden" ;
    rdfs:comment "Een shape met twee eigenschappen: één om een keuzelijstveld te creëren waarmee de gebruiker een filter op een kwartaal kan specificeren, en een tweede voor handmatige invoer om het jaar te specificeren."@nl ;
    sh:property [
          sh:path ex:kwartaal ;
          sh:name "kwartaal";
          sh:description "De keuzelijst met waarden voor kwartalen."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:string ;
          sh:in ("Q1" "Q2" "Q3" "Q4")
      ] ;
    sh:property [
          sh:path ex:jaar ;
          sh:name "jaar";
          sh:description "Handmatige invoer van de jaarwaarde na de kwartaalwaarden."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:gYear ;
          sh:maxInclusive "2025"^^xsd:gYear ;
          sh:minInclusive "2025"^^xsd:gYear ;
      ]
.

# De volgende SHACL shape definieert de invoerparameters voor de startperiode..
ex:StartDateManualEntry
    a sh:NodeShape ;
    sh:targetClass ex:StartDateManualEntry ;
    rdfs:label "Startdatum zonder keuzelijst";
    rdfs:comment "Een shape voor het creëren van een handmatig invoerveld waarmee de gebruiker de filter voor de startdatum kan specificeren."@nl ;
    sh:property [
          sh:path ex:startperiode ;
          sh:name "startperiode";
          sh:description "Handmatige invoer van de startdatumwaarde voor de periode."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:date ;
          sh:maxInclusive "2025-12-31"^^xsd:date ;
          sh:minInclusive "2025-01-31"^^xsd:date ;
      ]
.

# De volgende SHACL shape definieert de invoerparameters voor de eindperiode.
ex:EndDateManualEntry
    a sh:NodeShape ;
    sh:targetClass ex:EndDateManualEntry ;
    rdfs:label "Einddatum zonder keuzelijst";
    rdfs:comment "Een shape voor het creëren van een handmatig invoerveld waarmee de gebruiker het filter op de einddatum kan specificeren."@nl ;
    sh:property [
          sh:path ex:eindperiode ;
          sh:name "eindperiode";
          sh:description "Handmatige invoer van de einddatumwaarde voor de periode."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:date ;
          sh:maxInclusive "2025-12-31"^^xsd:date ;
          sh:minInclusive "2020-12-31"^^xsd:date ;
      ]
.

# De volgende SHACL shapes definiëren de invoerparameters voor de start- en einddatums samen.
ex:StartAndEndDateManualEntry
    a sh:NodeShape ;
    sh:targetClass ex:StartAndEndDateManualEntry ;
    rdfs:label "Start- en einddatum zonder keuzelijst";
    rdfs:comment "Een shape met twee eigenschappen met dezelfde formaten: één voor het creëren van een handmatig invoerveld waarmee de gebruiker de filter voor de startdatum kan specificeren, en een tweede voor de einddatum."@nl ;
    sh:property [
          sh:path ex:startperiode ;
          sh:name "startperiode";
          sh:description "Handmatige invoer van de startdatumwaarde."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:date ;
          sh:maxInclusive "2025-12-31"^^xsd:date ;
          sh:minInclusive "2020-12-31"^^xsd:date ;
      ],[
          sh:path ex:eindperiode ;
          sh:name "eindperiode";
          sh:description "Handmatige invoer van de einddatumwaarde."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:date ;
          sh:maxInclusive "2025-12-31"^^xsd:date ;
          sh:minInclusive "2020-12-31"^^xsd:date ;
      ]
.

# De volgende SHACL shape definieert de invoerparameter voor Vestigingsnummer met handmatige invoer als een string..
ex:VestigingsnummerManualEntry
    a sh:NodeShape ;
    sh:targetClass ex:VestigingsnummerManualEntry ;
    rdfs:label "Vestigingsnummer voor handmatige invoer";
    rdfs:comment "Een shape voor het creëren van een handmatig invoerveld waarmee de gebruiker het KvK Vestigingsnummer kan specificeren."@nl ;
    sh:property [
          sh:path ex:vestigingsnummer ;
          sh:name "vestigingsnummer";
          sh:description "Handmatige invoer van het KvK vestigingsnummer."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:datatype xsd:string ;  
          sh:pattern "^[0-9]{12}$" ; 
    ]
.

# De volgende SHACL shape definieert de invoerparameter voor Zorgkantoor IRI.
ex:Zorgkantoor
    a sh:NodeShape ;
    sh:targetClass ex:Zorgkantoor ;
    rdfs:label "Zorgkantoor" ;
    rdfs:comment "Een shape voor het kunnen meegeven van de specifieke zorgkantoor IRI."@nl ;
    sh:property [
          sh:path ex:zorgkantoor ;
          sh:name "zorgkantoor" ;
          sh:description "De betreffende IRI van het zorgkantoor."@nl ;
          sh:maxCount 1 ;
          sh:minCount 1 ;
          sh:nodeKind sh:IRI ;
          sh:in (
              <http://purl.org/ozo/onz-org#ZorgkantoorCZ>
              <http://purl.org/ozo/onz-org#ZorgkantoorDSW>
              <http://purl.org/ozo/onz-org#ZorgkantoorFriesland>
              <http://purl.org/ozo/onz-org#ZorgkantoorMenzis>
              <http://purl.org/ozo/onz-org#ZorgkantoorMiddenIJssel>
              <http://purl.org/ozo/onz-org#ZorgkantoorVGZ>
              <http://purl.org/ozo/onz-org#ZorgkantoorZilverenKruis>
              <http://purl.org/ozo/onz-org#ZorgkantoorZorgEnZekerheid>
          ) ;
    ]
.
```
