---
title: Inhoudsopgave
weight: 1
---
# Inhoudsopgave
* [Hoofdstuk 1: Inleiding](/Documentatie/Inleiding/_index.md)
* [Hoofdstuk 2: Procesomschrijving](/Documentatie/Procesomschrijving/_index.md)
* [Hoofdstuk 3: Oplossingsrichting](/Documentatie/Oplossingsrichting/_index.md)
* [Hoofdstuk 4: Data specificaties](/Documentatie/Data%20specificaties/_index.md)
* [Hoofdstuk 5: Implementatie](/Documentatie/Implementatie/_index.md)
* [Hoofdstuk 6: Aanvullende specificaties](/Documentatie/Aanvullende%20specificaties/_index.md)
* [Hoofdstuk 7: Onderwerpen voor doorontwikkeling](/Documentatie/Onderwerpen%20voor%20doorontwikkeling/_index.md)

## Verdere informatie
* **Toelichting:** Voorstel van de leveranciersspecificatie voor van KIK-V, versie 1.0. Het document is vooralsnog niet volledig en vormt een aanzet om tot werkende en sluitende afspraken te komen. De komende maanden worden er voor de openstaande vraagstukken nog aanvullende specificaties uitgewerkt en huidige specificaties potentieel nog aangescherpt. Het voorstel is bedoeld voor bredere consultatie, oriëntatie van partijen en praktijktoetsing.
* Dit werk valt onder een [Creative Commons Naamsvermelding-GelijkDelen 4.0 Internationaal-licentie](https://creativecommons.org/licenses/by-sa/4.0/). 
© Copyright (2022) Zorginstituut Nederland.
