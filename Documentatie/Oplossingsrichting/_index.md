---
title: Hoofdstuk 3 - Oplossingsrichting
weight: 4
---
# Hoofdstuk 3 - Oplossingsrichting
Onderstaande paragrafen geven een oplossingsrichting voor het proces dat beschreven is in het vorige hoofdstuk. We nemen hierbij het Nuts manifest als leidraad en baseren ons op de Nuts-specificaties zoals beschreven in de [RFC’s](https://nuts-foundation.gitbook.io/drafts/). Een implementatie van deze specificaties wordt een Nuts-node genoemd en samen vormen alle Nuts-nodes een vertrouwd netwerk. De technische specificatie behelst het verzenden, ontvangen, verwerken en beantwoorden van gevalideerde vragen in het vertrouwde netwerk. 

## ​3.1​. Berichtendienst
Het uitgangspunt in het ontwerp van deze technische specificatie is een berichtendienst. Dit houdt in dat vraag en antwoord als berichten worden verzonden, asynchroon. Reden voor het gebruik van een berichtendienst is dat tussen de vraag en het antwoord een termijn van beantwoording zit. Het ontwerp van de berichtendienst is generiek van opzet zodat hergebruik van de berichtendienst mogelijk is voor andere use cases.

Verzenders en ontvangers van berichten moeten een berichtendienst realiseren en zijn verantwoordelijk voor het vaststellen van de eisen aan deze berichtendienst. Verzenders moeten hierbij rekening houden dat een bericht niet afgeleverd kan worden. Een ontvangende partij kan op enig moment niet beschikbaar zijn. In de berichtendienst moet daarom een proces zijn ingericht voor het herhaaldelijk verzenden van berichten inclusief een escalatie naar een handmatig proces. Indien het bericht na verloop van tijd nog steeds niet kan worden afgeleverd moet een escalatie plaatsvinden zodat de verzender aan de afspraken uit het uitwisselprofiel kan blijven voldoen. 

## 3.2.​ Taal voor kennisrepresentatie
Afspraak is dat de OWL 2 Web Ontology Language [https://www.w3.org/TR/sparql11-overview/](https://www.w3.org/TR/sparql11-overview/) wordt toegepast als taal voor kennisrepresentatie. Dit is conform DIZRA-principe 9 over open internationale standaarden en de FAIR-data principes (DIZRA-principe 6). De data zelf wordt weergegeven overeenkomstig het Resource Description Framework. Aanbieders moeten datadiensten op basis van deze standaarden aanbieden. De data wordt bewaard in een RDF-database. Hiervoor zijn zowel open source als commerciële alternatieven beschikbaar.

## 3.3.​ Technische vraagstelling
De technische vraagstelling wordt geïmplementeerd in SPARQL versie 1.1, zie [https://www.w3.org/TR/sparql11-overview/](https://www.w3.org/TR/sparql11-overview/). SPARQL is de standaard query language en protocol voor Linked Open Data en RDF.

## 3.4.​ Doorzoekbaar register
Daar waar in de processen gesproken wordt over een doorzoekbaar register, worden de gedistribueerde registers bedoeld in het vertrouwde Nuts-netwerk.

## 3.5.​ Wetgeving
In de Afsprakenset KIK-V wordt de relevante wet- en regelgeving voor de uitwisseling van informatie (over kwaliteit en bedrijfsvoering) in de verpleeghuiszorg opgesomd in het [juridisch kader](https://kik-v-publicatieplatform.nl/afsprakenset/_/content/docs/juridisch-kader). Relevante wet- en regelgeving zijn onder andere de AVG, WKKGZ, de Wet aanvullende bepalingen verwerking persoonsgegevens in de zorg en het Besluit elektronische gegevensverwerking door zorgaanbieders. De technische specificaties geven op gestandaardiseerde wijze invulling aan de van toepassing zijnde wetgeving.

## ​3.6.​ Beveiliging & vertrouwen
KIK-V gaat uit van een decentraal netwerk waarin iedere partij in het netwerk haar eigen verantwoordelijkheid heeft ten aanzien van de inrichting van haar gegevensvoorziening. De eisen aan beveiliging en vertrouwen worden daarom door de partijen zelf vastgesteld net als alle andere eisen. Voor zorginstellingen is van toepassing dat zij moeten voldoen aan de NEN-normen 7510 en 7512.

KIK-V maakt afspraken over interoperabiliteit tussen de ketenpartijen zodat er een matching plaatsvindt op de eisen aan onder andere beveiliging en vertrouwen. In KIK-V spreken we af dat de ontvangende berichtendienst de volgende zaken moet kunnen controleren:

1. Welke verzendende dienst (“de verzender”) verbinding maakt
2. Namens welke rechtspersoon de verzender verbinding maakt

Ervan uitgaande dat er behoefte is aan een zo open mogelijke oplossing, waarbij elke partij evenveel kansen krijgt in de markt (conform DIZRA-principe 4: "gelijk speelveld voor alle leveranciers") en waarbij de beveiliging van een erg hoog niveau is, is gestandaardiseerd gebruik van cryptografische bewijzen noodzakelijk. Elke van de bovengenoemde controles wordt met behulp van een cryptografische handtekening uitgevoerd. Het ontwerp volgt hiermee de lijn van de tweede versie van de Verordening “Electronic Identities And Trust Services” (eIDAS2) en de Wet digitale overheid.

## ​3.7.​ Open & inclusief
We kiezen in het ontwerp van de technische specificaties voor open standaarden overeenkomstig DIZRA-principe 9. Er worden geen verplichtingen gesteld tot het gebruik van bepaalde services of software. Elke partij is vrij om te kiezen of ze gebruikmaken van ondersteunende diensten zolang deze diensten geen eisen of restricties opleggen aan andere partijen.

In de basis betekent dit dat er uitgegaan wordt van een gedistribueerde oplossing, waarbij elke partij gelijk is en waarbij zonder tussenkomst van derden, twee partijen rechtstreeks met elkaar gegevens kunnen delen inzake een bepaalde zorgtoepassing.