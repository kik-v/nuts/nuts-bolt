---
title: Hoofdstuk 6 - Aanvullende specificaties
weight: 7
---
# Hoofdstuk 6 - Aanvullende specificaties
## ​6.1.​ Verzenden berichten via de berichtendienst
De specificatie van de berichtendienst is gebaseerd op de [didcomm-specificatie](https://identity.foundation/didcomm-messaging/spec/#threads) voor een DIDComm signed message. Dit is een uitbreiding van de standaarden voor Javascript Object Signing and Encryption (JOSE). Zowel aanbieders als afnemers moeten een berichtendienst beschikbaar stellen voor het ontvangen en versturen van berichten.

![Gevalideerde vraag](Afbeelding_5.png)
*Figuur 5. Verzenden berichten via de berichtendienst.*

### ​6.1.1.​ Vinden adres van de berichtendienst
De verzender moet het service-adres (endpoint) van de berichtendienst van de ontvanger kunnen vinden. Om het service-adres te kunnen vinden moet een ontvanger de berichtendienst beschikbaar hebben gesteld en geregistreerd hebben als Nuts Endpoint voor de ontvangst van gevalideerde vragen. Zie hiervoor de paragraaf omtrent [adressering van de berichtendienst](/Documentatie/Aanvullende%20specificaties#6.3.-adressering-berichtendienst).

### ​6.1.2.​ Aanvragen autorisatie
Zie hiervoor de paragraaf over de [beveiliging van de berichtendienst](/Documentatie/Aanvullende%20specificaties#6.4.-beveiliging-van-de-berichtendienst).

### 6.1.3.​ Samenstellen inhoud bericht
De inhoud van het bericht bevat verschillende onderdelen. Hieronder is schematisch weergegeven welke onderdelen er zijn en welke door wie verzegeld moet worden.

![Schema](Afbeelding_6.png)

Een DIDComm-bericht bestaat in de kern uit een DIDComm plaintext message. Hierin is de data, zoals een gevalideerde vraag en het antwoord, opgenomen. 

Het plaintext message kan elektronisch verzegeld worden door de verzender (JWS envelope) als onweerlegbaar bewezen moet worden dat de verzender het bericht heeft verzonden. Naast de JWS envelope kan ook gekozen worden voor versleuteling van het bericht (JWE envelop). Op dit moment gaan we niet uit van verzegeling en versleuteling op berichtniveau omdat berichten direct tussen verzender en ontvanger worden verzonden zonder tussenkomst van derden. De onweerlegbaarheid, vertrouwelijkheid en integriteit hoeft op dit moment niet gewaarborgd te worden voor derden. Beveiliging van het berichtenverkeer vindt immers al plaats door mTLS. Zowel de afnemer als de aanbieder zijn in deze verzender en ontvanger.

#### ​6.1.3.1.​ Specificatie van het bericht
Zie voor de betekenis van de header-velden de [didcomm-header-specificatie](https://identity.foundation/didcomm-messaging/spec/#threads) voor een DIDComm plaintext message. De berichten voor gevalideerde vragen worden verzonden volgens het “validated-query-request”-protocol. Dit is een KIK-V protocol. Het protocol is te herkennen aan het type bericht. Het type bericht geeft eveneens aan welk bericht het betreft binnen het protocol.

Voor de berichten worden de volgende types gehanteerd: 
* Vraagbericht
* Antwoordbericht

Het protocol voor de toepassing in KIK-V gaat ervan uit dat een bericht altijd een uniek 1-op-1 bericht moet zijn zonder routering door derden. De attributen voor verzender (“from”) en ontvanger (“to”) zijn verplicht. De verzender is eveneens te identificeren door de “verifiable presentation”.

**Samenstellen vraagbericht**
Ieder bericht is te identificeren door de id. Dit moet een globale unieke identificatie zijn, versie 4 (UUIDv4), geformatteerd overeenkomstig de urn:uuid definitie. De created_time geeft aan wanneer het bericht is aangemaakt (in UTC) door de verzender, niet wanneer het is verzonden.

De presentatie van de gevalideerde vraag moet opgenomen zijn in het attribuut “vp”. 

Voorbeeld (niet normatief):

```
{
  "id": "urn:uuid:fe1774f5-e117-48b9-b6da-5e665ce3d821",
  "type": "https://www.kik-v.nl/validated-query-request/1.0/request",
  "created_time": 1516269022,
  "body": {
      “vp”: “Uitgaande van een JSON-LD presentation, base64 encoded string van de verifiable presentation”
      “param_values”: “base64 encoded string van de parameterwaarden”
  }
}
```
Een vraagbericht moet voor de aanbieder onweerlegbaar afkomstig zijn van de afnemer. Dit wordt gegarandeerd door gebruik te maken van een verifieerbare presentatie van de verifieerbare referenties van de afnemer. De presentatie is verzegeld door de afnemer van de informatie. 

**Samenstellen antwoordbericht**
Het antwoordbericht van een aanbieder wordt als antwoord verzonden op het vraagbericht van de afnemer. Door middel van het attribuut “thid” wordt verwezen naar het vraagbericht zodat de afnemer de vraag aan het antwoord kan relateren. De created_time geeft aan wanneer het bericht is aangemaakt (in UTC) door de verzender, niet wanneer het is verzonden.

Het is toegestaan om meerdere antwoordberichten te verzenden op basis van een vraagbericht. In de resultset van een antwoord wordt verwezen naar de gevalideerde vraag waar het antwoord betrekking op heeft. Ook is het toegestaan dat een vraag meerdere keren wordt beantwoord zodat correcties doorgegeven kunnen worden. Het laatste antwoord is de geldige.

De json met de resultset wordt opgenomen in de body van het bericht.

Voorbeeld (niet normatief):
```
{
  "id": "urn:uuid:77fcaa6b-7df0-4e7d-9c96-c407de7446df",
  "thid": "urn:uuid:fe1774f5-e117-48b9-b6da-5e665ce3d821",
  "type": "https://www.kik-v.nl/validated-query-request/1.0/response",
  "from": "did:nuts:1234567890A",
  "to": ["did:nuts:1234567890B"],
  "created_time": 1516269022,
  "body": {
      “response”: “JSON Web Signature met het antwoord op de vraag”
  }
}
```

### ​6.1.4.​ Samenstellen foutbericht als reactie op het vraagbericht
In de didcomm-specificatie is de werkwijze opgenomen voor het verzenden van foutrapporten. De attribuutcode in deze specificatie geeft de foutcode aan die aan de verzender van het vraagbericht wordt teruggegeven. De code is opgebouwd uit een aantal onderdelen zoals in de specificatie is weergegeven.

In onderstaande tabel zijn de waarden weergegeven van de foutcodes voor de toepassing in KIK-V.

| Waarde | Toelichting |  |
|---|---|---|
| e | “sorter”, zijnde een e voor error. |  |
| p | “scope”, zijnde p voor protocol-scope of m voor message-scope Voor KIK-V wordt uitgegaan van foutmeldingen op protocol-scope omdat met een foutmelding het protocol stopt. |  |
| descriptors | In de didcomm-specificatie zijn standaardwaarden opgenomen voor foutcodes.  trust.crypto wordt bijvoorbeeld gebruikt om aan te geven dat een credential of presentation niet geldig is. Daarnaast worden de volgende specifieke foutcodes toegevoegd. **req.out-of-band**: Hiermee kan een aanbieder aangeven dat zij geen antwoord zal geven via de berichtendienst. De aanbieder en afnemer nemen op basis hiervan contact met elkaar op om tot verdere afspraken te komen. |  |

### 6.1.5.​ Verzenden en ontvangen bericht
Het bericht wordt als een http bericht verzonden zoals in onderstaand voorbeeld weergegeven. De access token moet in de Authorization header van het http bericht worden opgenomen, voorafgegaan met “Bearer “. De host moet vervangen worden door het service-endpoint van de messaging-service (zie [adressering berichtendienst](/Documentatie/Aanvullende%20specificaties#6.3.-adressering-berichtendienst)).

```
POST /messaging HTTP/1.1
Host: https://didcomm.example.com 
Accept: application/json
Authorization: Bearer mF_9.B5f-4.1JqM
Content-Type: application/didcomm-plain+json

{
  Bericht in body
}
```

Bij ontvangst moet de ontvanger de access token op geldigheid verifiëren. Zie hiervoor de paragraaf over [beveiliging van de berichtendienst](/Documentatie/Aanvullende%20specificaties#6.4.-beveiliging-van-de-berichtendienst).

Bij ontvangst van een antwoordbericht moet de ontvanger verifiëren dat de ‘thid’ van het antwoordbericht gelijk is aan de ‘id’ van een verzonden bericht.

### ​6.1.6​. Ontvangstbevestiging voor een bericht
Een vraag- en antwoordbericht worden door een ‘POST’ verzonden aan een ontvanger. De ontvangst moet bevestigd worden door middel van html response code ‘202 Accepted’. 

Indien een foutmelding wordt ontvangen - een response code in de 400- en 500-reeks - dan moet de verzender het verzenden op een later tijdstip herhalen dan wel contact opnemen met de ontvanger c.q. dienstverlener van de ontvanger.

## ​6.2.​ Logboek van ontvangen en verzonden berichten
Voor de berichtendienst moet een logboek van ontvangen (“inbox”) en verzonden (“outbox”) berichten worden geregistreerd. Voor het logboek worden de volgende attributen geregistreerd:

* id - Unieke identificatie van het bericht, geformatteerd overeenkomstig de urn:uuid definitie.
Voorbeeld: urn:uuid:27365d58-7cfe-43d7-87f3-1b8ef4a79fdd
* thid - Identificeert op unieke wijze de thread waartoe het bericht behoort. Een antwoord- en foutbericht gebruikt de ‘thid’ om te identificeren dat het bericht betrekking heeft op het vraagbericht met ‘id’. Er kunnen meerdere antwoorden gegeven worden op een vraagbericht.
Voorbeeld: urn:uuid:27365d58-7cfe-43d7-87f3-1b8ef4a79fdd
* type - type bericht zijnde vraagbericht, antwoordbericht of foutbericht.
* timestamp_received - Aanduiding van het moment waarop het bericht is ontvangen in UTC (voor inbox) volgens het [ISO 8601 formaat](https://www.w3.org/TR/NOTE-datetime).
Voorbeeld: “2022-07-01T12:22:02Z”
* timestamp_sent - Aanduiding van het moment waarop het bericht is verzonden in UTC (voor outbox) volgens het [ISO 8601 formaat](https://www.w3.org/TR/NOTE-datetime).
Voorbeeld: “2022-07-01T14:03:02Z”
* from - Unieke referentie naar de did van de organisatie die het bericht heeft verzonden (voor inbox). gebaseerd op het ‘access token’ dat in de context van het bericht ontvangen is. 
Voorbeeld: “did:nuts:1234567890A”
* to - Unieke referentie naar de did van de ontvanger van het bericht (voor outbox)
Voorbeeld: “did:nuts:1234567890B”
* body - De body van het bericht
* attachments - Eventuele attachments van het bericht, zie de [didcomm specificatie](https://identity.foundation/didcomm-messaging/spec/#attachments) voor uitleg over attachments.

## 6.3.​ Adressering berichtendienst
Voor het vinden van de berichtendienst van een verzender en ontvanger wordt gebruikgemaakt van het Nuts register volgens [RFC 006](https://nuts-foundation.gitbook.io/drafts/rfc/rfc006-distributed-registry) van de Nuts specificatie. Zowel de verzendende partij als de ontvangende partij moet een dienst registreren in het register. In KIK-V zijn de afnemer en de aanbieder zowel verzender als ontvanger, afhankelijk of het de informatievraag betreft dan wel het antwoord.

### 6.3.1.​ Registreren berichtendienst
Diensten kunnen per leverancier en/of organisatie geregistreerd worden volgens de service specificatie. Voor de verzendende partij dient er een compound service genaamd didcomm-service-kikv geregistreerd te worden:

```
{
  "id": "did:nuts:organization_identifier#F1Dsgwngfdg3SH6TpDv0Ta1aOE",
  "type": "didcomm-service-kikv", 
  "serviceEndpoint": {
    "oauth": "did:nuts:vendor_identifier/serviceEndpoint?type=production-oauth",
    "didcomm": "did:nuts:vendor_identifier/serviceEndpoint?type=didcomm-messaging-kikv"
  }
}
```

De service moet een dynamische verwijzing hebben naar een endpoint zoals gespecificeerd in [RFC 006](https://nuts-foundation.gitbook.io/drafts/rfc/rfc006-distributed-registry). Het type van het endpoint moet ‘didcomm-messaging-kikv’ zijn. Het endpoint verwijst naar de berichtendienst zoals beschreven in [paragraaf 6.1](/Documentatie/Aanvullende%20specificaties#6.1.-verzenden-berichten-via-de-berichtendienst) van deze technische specificatie. De dynamische verwijzingen uit het voorbeeld verwijzen bijvoorbeeld naar de endpoint die de leverancier geregistreerd heeft:

```
{
    "id": "did:nuts:vendor_identifier#F1Dsgwngfdg3SH6TpDv0Ta1aOE",
    "type": "didcomm-messaging-kikv", 
    "serviceEndpoint": "https://didcomm.example.com/messaging"
}
```

### ​6.3.2.​ Vinden berichtendienst
Als de berichtendienst correct is geregistreerd, kunnen partijen in het netwerk deze vinden. Voor meer informatie over het registreren en het vinden van services, zie de [Nuts documentatie](https://nuts-node.readthedocs.io/en/stable/pages/getting-started/6-connecting-ehr.html).

| Let op! |
|---|
| Het programma KIK-V onderzoekt nog het vinden van een zorginstelling en haar services op basis van het KvK-nummer of het AGB-nummer. Hiervoor moet het uittreksel zorgaanbiedergegevens geïmplementeerd zijn (zie ontwikkelagenda, paragraaf 7.1.2 Uittreksel zorgaanbiedergegevens). Een tussentijdse oplossing is het zoeken van een zorginstelling op basis van de naam zoals deze in het NutsOrganizationCredential is opgenomen. |

## 6.4.​ Beveiliging van de berichtendienst
Een access token wordt gebruikt voor de beveiliging van de berichtendienst. De verzender moet een access token aanvragen voor de berichtendienst van de ontvanger. De beveiliging van de berichtendienst zal worden uitgevoerd overeenkomstig [RFC 003](https://nuts-foundation.gitbook.io/drafts/rfc/rfc003-oauth2-authorization). Voor de berichtendienst is echter geen identificatie van de eindgebruiker vereist. Deze hoeft dan ook niet meegegeven te worden in de aanvraag voor een access token. Als het access token niet geldig is, wordt het bericht niet geaccepteerd en is het niet ontvangen. 

Onderstaand is een voorbeeld voor een aanvraag van een access token voor het verzenden van een gevalideerde vraag (niet normatief).

```
POST http://example.com/internal/auth/v1/request-access-token
Content-Type: application/json

{
    "authorizer": "did:nuts:<aanbieder DID>",
    "requester": "did:nuts:<afnemer DID>",
    "service": "didcomm-service-kikv"
}
```

Diensten kunnen per leverancier en/of organisatie geregistreerd worden volgens de service specificatie. Voor beveiliging van de berichtendienst dient er een “oauth”-service geregistreerd te worden. Deze moet in combinatie met de compound service voor didcomm geregistreerd worden.

```
{
  "id": "did:nuts:organization_identifier#F1Dsgwngfdg3SH6TpDv0Ta1aOE",
  "type": "didcomm-service-kikv", 
  "serviceEndpoint": {
    "oauth": "did:nuts:vendor_identifier/serviceEndpoint?type=production-oauth",
    "didcomm": "did:nuts:vendor_identifier/serviceEndpoint?type=didcomm-messaging-kikv"
  }
}
```

Het endpoint waarnaar verwezen wordt vanuit het oauth veld moet een serviceEndpoint hebben. Deze verwijst naar het “access token”-endpoint van een OAuth authentication server. Het type in het query veld mag daarbij door de leverancier zelf gekozen worden. De dynamische verwijzingen uit het voorbeeld verwijzen bijvoorbeeld naar de endpoints die de leverancier geregistreerd heeft:

```
{
    "id": "did:nuts:vendor_identifier#F1Dsgwngfdg3SH6TpDv0Ta1aOE",
    "type": "production-oauth",
    "serviceEndpoint": "https://nuts.example.com/n2n/auth/v1/accesstoken"
}
```

## 6.5.​ Berichtversleuteling
Uitgangspunt voor KIK-V is dat de antwoorden op gevalideerde vragen geen persoonsgegevens bevatten. Daarom wordt vooralsnog geen berichtversleuteling toegepast. De [didcomm-specificatie](https://identity.foundation/didcomm-messaging/spec/#didcomm-encrypted-messages) staat toe dat versleuteling kan worden toegepast. In de toekomst kan daarom op enig moment alsnog besloten worden tot versleuteling.

## 6.6.​ Kanaalversleuteling met tweezijdige authenticatie
Alle http requests worden gedaan over een mTLS verbinding. Er wordt gebruikgemaakt van PKIoverheid private servercertificaten die verkrijgbaar zijn bij een vertrouwensdienstverlener (TSP), zie [Aanvraag PKIoverheidscertificaat](https://www.logius.nl/domeinen/toegang/pkioverheidcertificaat-aanvragen). De eigenaar (of haar bevoegde vertegenwoordiger) van het Internetdomein waarop de endpoints van het datastation worden gehost moet de aanvrager zijn van het servercertificaat. Dit kan een andere aanvrager zijn dan de aanvrager van het PKIOVerheid private servercertificaat van de Nuts node. De Nuts node kan immers op een ander internetdomein worden gehost met een andere eigenaar dan wel bevoegde vertegenwoordiger. Zie ook [RFC 008](https://nuts-foundation.gitbook.io/drafts/rfc/rfc008-certificate-structure).