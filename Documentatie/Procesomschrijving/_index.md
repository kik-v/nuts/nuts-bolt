---
title: Hoofdstuk 2 - Procesomschrijving
weight: 3
---
# Hoofdstuk 2 - Procesomschrijving
Deze specificatie ondersteunt het proces voor gegevens-uitvragen over kwaliteit en bedrijfsvoering door afnemers bij zorginstellingen met verpleeghuiszorg (aanbieders). Dit proces kent een aantal uitgangspunten:
* Afnemers en aanbieders komen onder begeleiding van de beheerorganisatie KIK-V gezamenlijk een informatievraag overeen, een gevalideerde vraag. Deze gevalideerde vraag wordt, al dan niet samen met andere gevalideerde vragen, opgenomen in een uitwisselprofiel. Dit uitwisselprofiel beschrijft de gegevensuitwisseling tussen een afnemer en aanbieders in een bepaalde uitwisselcontext;
* De beheerorganisatie KIK-V geeft per gevalideerde vraag een technisch bewijs uit aan een afnemer waaruit blijkt de afnemer een gevalideerde vraag mag stellen;
* Afnemers gebruiken het technische bewijs bij het daadwerkelijk stellen van de vraag aan een aanbieder om aan te tonen een vraag te mogen stellen;
* Aanbieders kunnen de echtheid van het technische bewijs zonder directe tussenkomst van de beheerorganisatie controleren, waardoor een derde partij geen directe rol hoeft te spelen in de daadwerkelijke uitwisseling. Het proces kent daardoor “een decentraal karakter”.

![Activiteitendiagram](Afbeelding_2.png)
*Figuur 2. Activiteitendiagram.*

## ​2.1​. Overeenkomen informatievraag en technische vraagstelling

Onder begeleiding van de beheerorganisatie KIK-V komt een afnemer met een informatievraag overeen met aanbieders. Dit gebeurt aan de hand van het matchingsproces zoals dat is opgenomen in [de Afsprakenset KIK-V](https://kik-v-publicatieplatform.nl/afsprakenset). Een informatievraag, die middels het matchingsproces tussen afnemer en aanbieders tot stand is gekomen, wordt ook wel een gevalideerde vraag genoemd.

De overeengekomen informatievraag, ofwel gevalideerde vraag, wordt al dan niet samen met andere gevalideerde vragen opgenomen in een uitwisselprofiel. Het uitwisselprofiel beschrijft, naast de gevalideerde vragen zelf, de relevante afspraken over de gegevensuitwisseling tussen afnemer en aanbieders in de betreffende uitwisselcontext. In het uitwisselprofiel wordt ook de technische uitwerking van de vraagstelling opgenomen.

Het uitwisselprofiel wordt, als uitkomst van het matchingsproces, vastgesteld via de governance van KIK-V. Vastgestelde uitwisselprofielen schrijft de beheerorganisatie KIK-V in in het register van uitwisselprofielen. Het register is een publiek register.

## ​2.2​. Uitgeven gevalideerde vraag
De beheerorganisatie KIK-V neemt het initiatief om technisch bewijs uit te geven waaruit blijkt dat de afnemer een gevalideerde vraag mag stellen aan een aanbieder. De beheerorganisatie KIK-V stelt hiervoor een dienst beschikbaar. 

De beheerorganisatie KIK-V kiest voor welk uitwisselprofiel en vervolgens voor welke technische vraagstellingen zij een gevalideerde vraag wil uitgeven. De beheerorganisatie KIK-V kiest een of meerdere technische vraagstellingen en kiest voor het uitgeven van een gevalideerde vraag. De dienst van de beheerorganisatie KIK-V geeft de gevalideerde vraag uit. Afspraak in deze is dat bij de daadwerkelijke gegevensuitwisseling de beheerorganisatie KIK-V door de aanbieder als uitgever van de gevalideerde vraag wordt vertrouwd.

De afnemer ontvangt de gevalideerde vraag in haar portemonnee voor digitale identiteit.

Een gevalideerde vraag kan op ieder moment door de beheerorganisatie KIK-V worden ingetrokken als zij daar een gegronde reden voor heeft.

![Uitgifte en gebruik van de gevalideerde vragen](Afbeelding_3.png)
*Figuur 3. Uitgifte en gebruik van de gevalideerde vragen.*

Uitgangspunt is dat een afnemer een gevalideerde vraag kan verkrijgen voor vragen over kwaliteits- en bedrijfsvoeringsinformatie.

## 2.3​. Uitgeven uittreksel zorgaanbiedergegevens
De beheerorganisatie KIK-V stelt een dienst beschikbaar waar een zorginstelling een uittreksel kan verkrijgen van haar geregistreerde zorgaanbiedergegevens. Het uittreksel bevat minimaal het KvK-nummer uit het Handelsregister en de AGB-nummers uit het AGB. Een zorginstelling met verpleeghuiszorg, die aanbieder is van data voor informatievragen, moet het initiatief nemen om het uittreksel te ontvangen. Afspraak in deze is dat de beheerorganisatie KIK-V als uitgever van het uittreksel wordt vertrouwd door de aanbieder in het vertrouwde netwerk.

| Let op! |
|---|
| De uitgifte van het uittreksel zorgaanbiedergegevens door de beheerorganisatie KIK-V betreft een tijdelijke oplossing, in lijn met de voorziene toekomstige oplossing. Op dit moment is landelijke ontsluiting van authentieke bronnen, zoals het LRZa, (nog) niet ingericht. Er is daarom een andere, tijdelijke autoriteit nodig om de KIK-V use case werkend te krijgen via Nuts. De beheerorganisatie KIK-V vult deze rol voorlopig in. De informatie die de beheerorganisatie KIK-V in de rol van autoriteit ontsluit, dient ter ondersteuning van de eerste implementatiefase. Het uitgifteproces hiervan wordt nog nader uitgewerkt in afstemming met de beheerorganisatie, inclusief de condities waaronder dit gebeurt. |

De aanbieder ontvangt het uittreksel in haar portemonnee voor digitale identiteit, maar moet de attributen publiekelijk delen in een doorzoekbaar register zodat afnemers in staat zijn om op basis van (minimaal) het KvK-nummer de zorginstelling te vinden. Daarom zal de uitgever het uittreksel “public” uitgeven in het vertrouwde netwerk.

## ​2.4.​ Beschikbaar stellen data en adverteren endpoints
KIK-V volgt de DIZRA in het hanteren van de FAIR-dataprincipes. Een aanbieder moet daarom de data, die nodig is voor het beantwoorden van gevalideerde vragen, beschikbaar stellen in een formele, toegankelijke, gedeelde en breed toepasbare taal voor kennisrepresentatie. 

Door middel van gevalideerde vragen wordt de data toegankelijk gemaakt. De aanbieder stelt voor de ontvangst van gevalideerde vragen een berichtendienst beschikbaar en adverteert hiervan het endpoint.

## 2.5.​ Vinden zorginstelling
Een afnemer is zelf verantwoordelijk voor de doelgroepselectie voor een gegevensuitvraag. De afnemer kan hiervoor eigen registraties gebruiken of zich baseren op informatie geregistreerd in bijvoorbeeld het Landelijk Register Zorgaanbieders (LRZa). Met het uittreksel zorgaanbiedergegevens (zie paragraaf 2.3) legt de afnemer de verbinding tussen een te benaderen zorginstelling en de adressering in Nuts.

| Let op! |
|---|
| Het programma KIK-V onderzoekt nog op welke wijze zij afnemers kan ondersteunen bij het creëren van een totaaloverzicht van zorgaanbieders en het verkrijgen van de benodigde metadata voor een goede doelgroepselectie. |

## ​2.6.​ Verzenden gevalideerde vraag
Na het bepalen van de doelgroep, moet de afnemer in het vertrouwde netwerk de berichtendienst van de aanbieder kunnen adresseren (het technische adres van de dienst vinden). Met het uittreksel zorgaanbiedergegevens (zie paragraaf 2.3) is een verifieerbare relatie gerealiseerd tussen de identificerende gegevens van een zorginstelling en de adresseringsgegevens in Nuts. De afnemer kan daardoor bijvoorbeeld op basis van het KvK-nummer van de aanbieder de berichtendienst van deze aanbieder vinden.

De afnemer verzendt de gevalideerde vraag vervolgens naar de berichtendienst van de aanbieder. De gevalideerde vraag wordt gepresenteerd en elektronisch verzegeld door de afnemer. Voor iedere gevalideerde vraag in de presentatie moet een uniek conversatienummer gehanteerd worden voor de relatie tussen de vraag en het antwoord.

## ​2.7​. Ontvangen en beantwoorden gevalideerde vraag
De aanbieder ontvangt de gevalideerde vraag in haar berichtendienst. Na ontvangst verifieert de aanbieder dat de gevalideerde vraag geldig is, en dat de beheerorganisatie KIK-V de uitgever is. Afspraak is dat de aanbieder de beheerorganisatie KIK-V vertrouwt als uitgever van de gevalideerde vraag.

De aanbieder verwerkt de technische vraagstelling uit de gevalideerde vraag en verzendt het antwoord via de eigen berichtendienst naar de berichtendienst van de afnemer. Het antwoord moet elektronisch verzegeld zijn door de aanbieder en voorzien zijn van het conversatienummer van de afnemer. 

In het uitwisselprofiel waarin de gevalideerde vraag is opgenomen, kunnen aanvullende afspraken zijn opgenomen over de beantwoording door de aanbieder. Denk aan afspraken over bestuurlijke accordering voorafgaand aan het vrijgeven van het antwoord of afspraken over een beantwoordingstermijn. De aanbieder past deze afspraken toe bij het beantwoorden van de vraag en het verzenden van het antwoord.

Afspraak is dat de berichten in de berichtendienst niet verwijderd worden, maar als logboek gebruikt kan worden van ontvangen vragen en verzonden antwoorden.

| Let op! |
|---|
| Het programma KIK-V doet de aanbeveling dat het logboek van ontvangen vragen en verzonden antwoorden door een eindgebruiker handmatig ingekeken kan worden. Deze inkijkfunctie geeft de eindgebruiker de mogelijkheid om een audit trail te reconstrueren bij incidenten. |

## 2.8.​ Ontvangen antwoord op gevalideerde vraag
De afnemer ontvangt het antwoord op de gevalideerde vraag in haar berichtendienst. Na ontvangst verifieert de afnemer dat het antwoord afkomstig is van de aanbieder. Op basis van het conversatienummer kan de afnemer het antwoord aan de vraag relateren. De afnemer kan vervolgens de antwoorden verwerken in haar systemen.
