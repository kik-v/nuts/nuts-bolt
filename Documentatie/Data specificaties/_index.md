---
title: Hoofdstuk 4 - Data specificaties
weight: 5
---
# Hoofdstuk 4 - Data specificaties
Voordat een Nuts node een ValidatedQueryCredential kan valideren, moet de node door de beheerder worden geconfigureerd met de definitie van dit credential. Een gevalideerde vraag is een ‘custom credential’. De wijze waarop deze credentials (zijnde verifieerbare referenties) beschikbaar moeten worden gesteld staat onder [deze link](https://nuts-node.readthedocs.io/en/stable/pages/deployment/verifiable-credentials.html) beschreven. De definitie van de gevalideerde vraag moet in de configuratie gemapped worden met de [KIK-V JSON-LD context URL](https://gitlab.com/kik-v/nuts/specificatie-gevalideerde-vragen-credential/-/blob/main/specificaties_gevalideerde_vraag_credentials.jsonld?ref_type=heads).

In onderstaande tabel zijn de attributen van de gevalideerde vraag beschreven:

| Attributen | Toelichting |
|---|---|
| Identifier | De gevalideerde vraag heeft een unieke identificatie die bepaald wordt op het moment van aanmaken. Term: id Verwachte waarde: UUIDv4, String, geformatteerd overeenkomstig de urn:uuid definitie Voorbeeld: “urn:uuid:aaadb8ae-0cbc-4b10-ac41-57df3aedcc5a” |
| Name | Een logische naam voor de gevalideerde vraag Term: name Verwachte waarde: String Voorbeeld: “2.3.1 Ziekteverzuimpercentage” |
| Description | Een korte beschrijving van de gevalideerde vraag. Term: description Verwachte waarde: String Voorbeeld: “CBS/Vernet: Het ziekteverzuimpercentage is het totaal aantal ziektedagen van de personeelsleden, in procenten van het totaal aantal beschikbare (werk-/kalender) dagen van de werknemers in de verslagperiode. Het ziekteverzuimpercentage is inclusief het verzuim langer dan een jaar en exclusief zwangerschaps- en bevallingsverlof.” |
| Ontology | Een verwijzing naar de ontologieën waarmee de sparql query vraag is beschreven. Term: ontology. Verwachte waarde: Array van strings. Voorbeeld: [Ontology](https://kik-v-publicatieplatform.nl/ontologie/onz-zorg/) |
| Profile | Een verwijzing naar het uitwisselprofiel waarin de gevalideerde vraag is beschreven. Term: profile. Verwachte waarde: String. Voorbeeld: [Profile](https://kik-v-publicatieplatform.nl/documentatie/Uitwisselprofiel%20Zorginstituut%20Openbaarmaking%20kwaliteitsindicatoren%20verpleeghuiszorg/) |
| Sparql | De afgesproken SPARQL-query voor de gevalideerde vraag. Term: sparql Verwachte waarde: Base64 encoded string. Zie voorbeeld 1.|
| SHACL voor de parameters | De definitie van de parameters die in de SPARQL-query kunnen zijn opgenomen. De aanbieder ontvangt de parameters in het bericht van de gevalideerde vraag. Aan de hand van de definitie van de parameters kan de aanbieder de geldigheid van de parameters verifiëren.  De definitie wordt in de gevalideerde vraag opgenomen en beschreven met de W3C Shapes Constraint Language (SHACL). Term: parameters Verwachte waarde: Base64 encoded string. Zie voorbeeld 2. |

```
#Voorbeeld 1:
UFJFRklYIHZwaDogPGh0dHBzOi8vcHVybC5vcmcvb3pvL3ZwaCM+ClBSRUZJWCBraWs6IDxodHRwczovL3B1cmwub3JnL296by9raWsjPgpQUkVGSVggeHNkOiA8aHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEjPgoKU0VMRUNUIChTVU0oP2RhZ2VuX3ppZWt0ZSkgQVMgP3RvdGFhbF96aWVrKSAoU1VNKD9kYWdlbl9vbXZhbmcpIEFTID90b3RhYWxfd2VyaykgKD90b3RhYWxfemllay8/dG90YWFsX3dlcmsqMTAwIEFTID9pbmRpY2F0b3IpCgpXSEVSReKApg==
```

```
#Voorbeeld 2:
I1BhcmFtZXRlciBzaGFwZXMgZm9yIFBlaWxkYXR1bSB3aXRoIG1hbnVhbCBlbnRyeSBhcyBhIGRhdGUuDQpleDpQZWlsRGF0dW1NYW51YWxFbnRyeQ0KICAgIGEgc2g6Tm9kZVNoYXBlIDsNCiAgICBzaDp0YXJnZXRDbGFzcyBleDpRdWVyeVBhcmFtZXRlciA7DQogICAgcmRmczpsYWJlbCAiUGVpbERhdHVtIHdpdGhvdXQgYSBwaWNrIGxpc3QiOw0KICAgIHJkZnM6Y29tbWVudCAiQSBzaGFwZSBmb3IgY3JlYXRpbmcgYSBtYW51YWwgZW50cnkgZmllbGQgZm9yIHRoZSB1c2VyIHRvIHNwZWNpZnkgdGhlIFBlaWxEYXR1bS4iQGVuIDsNCiAgICBzaDpwcm9wZXJ0eSBbDQogICAgICAgICAgc2g6cGF0aCBleDpwZWlsZGF0dW0gOw0KICAgICAgICAgIHNoOm5hbWUgInBlaWxkYXR1bSI7DQogICAgICAgICAgc2g6ZGVzY3JpcHRpb24gIk1hbnVhbCBlbnRyeSBvZiBwZWlsZGF0dW0gdmFsdWVzLiJAZW4gOw0KICAgICAgICAgIHNoOm1heENvdW50IDEgOw0KICAgICAgICAgIHNoOm1pbkNvdW50IDEgOw0KICAgICAgICAgIHNoOmRhdGF0eXBlIHhzZDpkYXRlIDsNCiAgICAgICAgICBzaDptYXhJbmNsdXNpdmUgIjIwMjUtMTItMzEiXl54c2Q6ZGF0ZSA7DQogICAgICAgICAgc2g6bWluSW5jbHVzaXZlICIyMDIwLTEyLTMxIl5eeHNkOmRhdGUgOw0KICAgICAgXQ0KLg 
```

| Let op! |
|---|
| Het programma KIK-V werkt nog aan een URI-strategie. Daarom is nog geen geldige URI beschikbaar voor de definitie van de gevalideerde vraag. |

De verifiable credential moet voor een Nuts-node worden geconfigureerd met de volgende definitie: 

```
{
  "@context": {
    "@version": 1.1,
    "@protected": true,
    "id": "@id",
    "type": "@type",
    "schema": "http://schema.org/",
    "kikv": "https://example.org/kikv#",
    "validatedQuery": {
      "@id": "kikv:validatedQuery",
      "@type": "rdf:HTML"
    },
    "ValidatedQueryCredential": "kikv:validatedQueryCredential",
    "description": "schema:description",
    "identifier": "schema:identifier",
    "name": "http://schema.org/name",
    "ontology": ["kikv:kikvontology"],
    "profile": "kikv:profile",
    "sparql": "kikv:sparql",
    "paramsSHACL": "kikv:paramsSHACL"
  }
}
```

De geldigheidsperiode van een gevalideerde vraag is gebaseerd op de attributen “issuanceDate” en “expirationDate” uit Verifiable Credentials Data Model v1.1. Deze attributen moeten opgenomen zijn in de verifiable credential van een gevalideerde vraag en weergegeven worden volgens het [ISO 8601 formaat](https://www.w3.org/TR/NOTE-datetime). Een gevalideerde vraag is met andere woorden alleen geldig in de periode vanaf “issuanceDate” tot (en dus niet tot en met) “expirationDate”.